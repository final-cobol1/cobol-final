
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.  MYPROG.
       AUTHOR. NARANYA BOONMEE.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT  INPUT-FILE ASSIGN TO "trader7.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT INPUT-REPORT-FILE ASSIGN TO  "trader.rpt "   
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  INPUT-REPORT-FILE.
       01  PRINT-LINE PIC X(50).

       FD  INPUT-FILE. 
       01  INPUT-RECORD.
           88 END-OF-INPUT-FILE    VALUE  HIGH-VALUE .
           05 INPUT-PR             PIC X(2).
           05 INPUT-P-INCOME       PIC X(10).
           05 INPUT-MEMBER         PIC X(4).
           05 INPUT-M-INCOME       PIC X(10).
           05 MAX-PR               PIC X(2).
           05 SUM-INCOME           PIC (10).

       WORKING-STORAGE SECTION. 
       01  PAGE-NAME PIC X(50)
           VALUE "PROVINCE    P INCOME    MEMBER  MEMBER INCOME "
       01  INPUT-DETAIL-LINE.
           05 FILLER PIC  X VALUE SPACES .
           05 PAN-INPU-PR PIC X(2).
           05 FILLER PIC  X(8) VALUE SPACES .
           05 PAN-SUNM-P  PIC $$$$,$$9.
           05 FILLER  PIC X(5) VALUE SPACES .
           05 PAN-ID     PIC X(4).
           05 FILLER  PIC X(8) VALUE SPACES .
           05 PAN_MEM     PIC X(10).
       01  PAGE-TOOTER.
           05 FILLER  PIC X(15) VALUE "MAX PROVINCE:  ".
           05 PAN-GOOD PIC  X(2).
           05 FILLER PIC  2 VALUE SPACES .
           05 FILLER  PIC X(15) VALUE "MAX PROVINCE:  ".
           05 PAN-SUM-IN  PIC X(10).

       PROCEDURE DIVISION .
       

       
           



       



